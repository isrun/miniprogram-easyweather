const config = {
  name: '轻松天气',
  version: '1.3.6',
  versionInfo: '2020.08.16\n- 界面优化 -\n- 代码优化 -',
  request: {
    host: 'https://ali-weather.showapi.com',
    header: { 'Authorization': 'APPCODE ' + '将你的 AppCode 填到这里' },
  },
}

export default config;